public class Book{
    protected String title;
    private String author;

    //Constructor
    public Book(String title, String author){
        this.title = title;
        this.author = author;
    }

    //Getter methods
    public String getTitle(){
        return(this.title);
    }

    public String getAuthor(){
        return(this.author);
    }

    //toString() override
    public String toString(){
        return "The book " + this.title + " is written by " + this.author;
    }
}