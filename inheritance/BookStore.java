public class BookStore {
    public static void main(String[] args){
        //Create book array and create objects in the array
        Book[] books = new Book [5];

        books[0] = new Book("title1", "author1");
        books[1] = new ElectronicBook("title2", "author2", 500);
        books[2] = new Book("title3", "author3");
        books[3] = new ElectronicBook("title4", "author4", 2000);
        books[4] = new ElectronicBook("title5", "author5", 1500);

        //Print loop through the array
        for(int i=0; i<books.length; i++){
            System.out.println(books[i]);
        }
    }
}
