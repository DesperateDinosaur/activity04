public class ElectronicBook extends Book{
    private int numberBytes;

    //constructor
    public ElectronicBook(String title, String author, int numberBytes){
        super(title, author);
        this.numberBytes = numberBytes;
    }

    //Getter method
    public int getNumberBytes(){
        return this.numberBytes;
    }

    //toString() override
    public String toString(){
        return "The book " + this.title + " is written by " + getAuthor() + " and takes up " + this.numberBytes + " bytes of space";
    }
}
